import React from "react";
import api from "../instance";
import { formValues } from "./useGetForm";

export const useUpdateForm = () => {
  const [loading, setLoading] = React.useState<boolean>(false);

  const updateForm = async (inputsData: formValues) => {
    setLoading(true);
    await api.put("", inputsData);
    setLoading(false);
  };

  return { updateForm, loading };
};
