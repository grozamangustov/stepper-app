import React from "react";
import api from "../instance";

export interface formValues {
  id: string;
  inputA: number;
  inputB: number;
  inputC: number;
  inputD: number;
}

export const defaultValues = {
  id: "",
  inputA: 0,
  inputB: 0,
  inputC: 0,
  inputD: 0,
};

export const useGetForm = () => {
  const [loading, setLoading] = React.useState<boolean>(true);
  const [error, setError] = React.useState<Error | null>(null);
  const [data, setData] = React.useState<formValues>(defaultValues);

  const fetchData = async () => {
    try {
      setLoading(true);
      const result = await api.get("");
      setData(result.data);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      setError(error);
      return error;
    }
  };

  React.useEffect(() => {
    fetchData();
  }, []);

  return { data, loading, error };
};
