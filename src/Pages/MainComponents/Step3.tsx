import React from "react";
import { formValues } from "../../hooks/useGetForm";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import { useUpdateForm } from "../../hooks/useUpdateForm";

interface Input2Props {
  form: formValues;
  setActiveStep: Function;
}

export default function Step3(props: Input2Props) {
  const { updateForm, loading } = useUpdateForm();

  const handleNext = () => {
    if (!disabledButton()) {
      updateForm(props.form).then(() => {
        props.setActiveStep(() => 3);
      });
    }
  };
  const handleBack = () => {
    props.setActiveStep(() => 1);
  };

  const disabledButton = () => {
    if (props.form.inputC !== props.form.inputA + props.form.inputB) {
      return true;
    }
    if (
      props.form.inputD !==
      props.form.inputA + props.form.inputB + props.form.inputC
    ) {
      return true;
    }
    return false;
  };

  return loading ? (
    <CircularProgress />
  ) : (
    <div>
      Все готово! Вы уверены, что хотите отправить форму?
      <div style={{ marginTop: "10px" }}>
        <Button onClick={handleBack} style={{ marginRight: "10px" }}>
          Назад
        </Button>
        <Button
          disabled={disabledButton()}
          variant="contained"
          color="primary"
          onClick={handleNext}
        >
          Отправить форму
        </Button>
      </div>
    </div>
  );
}
