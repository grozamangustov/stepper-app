import React, { useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { formValues } from "../../hooks/useGetForm";

interface Input1Props {
  form: { data: formValues; setForm: Function };
  setActiveStep: Function;
}

type errorsVar = {
  inputA?: string;
  inputB?: string;
  inputC?: string;
};

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& > *": {
        margin: theme.spacing(1),
        width: "50vw",
      },
    },
  })
);

export default function Step1(props: Input1Props) {
  const classes = useStyles();
  const [errors, setErrors] = useState<errorsVar>({});

  const changeInput = (e: any, inputName: string) => {
    props.form.setForm({
      ...props.form.data,
      [inputName]: Number(e.target.value),
    });
  };

  const handleNext = () => {
    const errorsValidation: errorsVar = {};
    if (props.form.data.inputA === 0) {
      errorsValidation.inputA = "Поле обязательно для заполнения";
    } else {
      delete errorsValidation.inputA;
    }
    if (props.form.data.inputB === 0) {
      errorsValidation.inputB = "Поле обязательно для заполнения";
    } else {
      delete errorsValidation.inputB;
    }
    if (
      props.form.data.inputC !==
      props.form.data.inputA + props.form.data.inputB
    ) {
      errorsValidation.inputC =
        "Поле inputC должно быть равно сумме inputA и inputB";
    } else {
      delete errorsValidation.inputC;
    }
    setErrors(errorsValidation);
    if (Object.keys(errorsValidation).length === 0) {
      props.setActiveStep(() => 1);
    }
  };

  return (
    <div>
      <form className={classes.root} noValidate autoComplete="off">
        <TextField
          id="InputA"
          label="InputA"
          value={props.form.data.inputA}
          type="number"
          error={!!errors.inputA}
          helperText={errors.inputA || ""}
          onChange={(e) => changeInput(e, "inputA")}
        />
        <TextField
          id="InputB"
          label="InputB"
          value={props.form.data.inputB}
          type="number"
          error={!!errors.inputB}
          helperText={errors.inputB || ""}
          onChange={(e) => changeInput(e, "inputB")}
        />
        <TextField
          id="InputC"
          label="InputC"
          value={props.form.data.inputC}
          type="number"
          error={!!errors.inputC}
          helperText={errors.inputC || ""}
          onChange={(e) => changeInput(e, "inputC")}
        />
      </form>
      <Button variant="contained" color="primary" onClick={handleNext}>
        Далее
      </Button>
    </div>
  );
}
