import React, { useState } from "react";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { formValues } from "../../hooks/useGetForm";
import Button from "@material-ui/core/Button";

interface Input2Props {
  form: formValues;
  setActiveStep: Function;
}

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& > *": {
        margin: theme.spacing(1),
        width: "50vw",
      },
    },
  })
);

export default function Step2(props: Input2Props) {
  const classes = useStyles();
  const [error, setError] = useState<string | null>(null);

  const handleNext = () => {
    if (
      props.form.inputD !==
      props.form.inputA + props.form.inputB + props.form.inputC
    ) {
      setError("inputD должен быть равен сумме inputA, inputB и inputC");
    } else {
      props.setActiveStep(() => 2);
    }
  };
  const handleBack = () => {
    props.setActiveStep(() => 0);
  };

  return (
    <div>
      <form className={classes.root} noValidate autoComplete="off">
        <TextField
          id="InputD"
          label="InputD"
          disabled
          error={!!error}
          helperText={error || ""}
          value={props.form.inputD}
        />
      </form>
      <Button onClick={handleBack} style={{ marginRight: "10px" }}>
        Назад
      </Button>
      <Button variant="contained" color="primary" onClick={handleNext}>
        Далее
      </Button>
    </div>
  );
}
