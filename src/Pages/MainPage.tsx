import React, { useEffect, useState } from "react";
import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Stepper from "../components/Stepper";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import Step1 from "./MainComponents/Step1";
import Step2 from "./MainComponents/Step2";
import Step3 from "./MainComponents/Step3";
import { defaultValues, formValues, useGetForm } from "../hooks/useGetForm";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
    },
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    instructions: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
    content: {
      flexGrow: 1,
      padding: "30px",
      marginTop: "50px",
    },
    card: {
      borderRadius: "10px",
      marginBottom: "30px",
      padding: "10px",
    },
  })
);

function getStepContent(
  step: number,
  setActiveStep: Function,
  form: { data: formValues; setForm: Function }
) {
  switch (step) {
    case 0:
      return (
        <Step1
          setActiveStep={setActiveStep}
          form={{ data: form.data, setForm: form.setForm }}
        />
      );
    case 1:
      return <Step2 setActiveStep={setActiveStep} form={form.data} />;
    case 2:
      return <Step3 setActiveStep={setActiveStep} form={form.data} />;
    default:
      return "Ошибка";
  }
}

export default function MainPage() {
  const classes = useStyles();
  const [activeStep, setActiveStep] = useState(0);
  const [form, setForm] = useState<formValues>(defaultValues);
  const steps = ["Шаг 1", "Шаг 2", "Конец!"];

  const { data, loading, error } = useGetForm();

  const handleReset = () => {
    setActiveStep(0);
  };

  useEffect(() => {
    if (data !== null) {
      setForm(data);
    }
  }, [data]);

  if (error) return <div>Произошла ошибка :( {error.message}</div>;
  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <Typography variant="h6" noWrap>
            Тестовое задание - степпер
          </Typography>
        </Toolbar>
      </AppBar>
      <main className={classes.content}>
        <Card className={classes.card}>
          <Stepper activeStep={activeStep} />
        </Card>
        {loading ? (
          <CircularProgress />
        ) : (
          <Card className={classes.card}>
            {activeStep === steps.length ? (
              <div>
                <Typography className={classes.instructions}>
                  Все прошло отлично! Вы можете обновить форму снова, если
                  желаете :)
                </Typography>
                <Button onClick={handleReset}>Заполнить заново</Button>
              </div>
            ) : (
              <div>
                <div>
                  {getStepContent(activeStep, setActiveStep, {
                    data: form,
                    setForm,
                  })}
                </div>
              </div>
            )}
          </Card>
        )}
      </main>
    </div>
  );
}
