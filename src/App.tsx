import React from "react";
import MainPage from "./Pages/MainPage";

import "./App.css";

function App() {
  return (
    <div className="App">
      <MainPage />
    </div>
  );
}

export default App;
