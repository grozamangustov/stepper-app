import axios from "axios";

export default axios.create({
  baseURL: "https://60361aba6496b900174a0050.mockapi.io/api/values/1",
  headers: {
    "Content-type": "application/json",
  },
});
